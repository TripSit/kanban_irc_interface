#!/usr/bin/env python3

import kanboard

kb = kanboard.Client('http://localhost/jsonrpc.php', 'jsonrpc', 'your_api_token')

# Create Project

project_id = kb.create_project(name='My project')

# add users to project

kb.add_project_user(project_id=project_id, user_id=123, role='project-manager')

# create task

task_id = kb.create_task(project_id=project_id, title='My task title')

# for eg for all this we need to:
# create user
# assign roles etc -> done through kd.add_project_user
# Create User
# https://docs.kanboard.org/en/latest/api/user_procedures.html#user-api-procedures

new_user_id = kb.createUser(username="preferably have the user's nick here",password="must use some sort of encyption", name="optional",email="optional, but we may choose to require this for notification purposes",role="string")

#create comment
# so this accepts 3 parameters acc to docs
# task_id, user_id, content

comment_id = kb.create_comment(task_id='whatever', user_id="need to capture this from the person sending all this is my work", content="this will be a variable for example which basically captures the input")

#the main thing is to figure out all the bits required and to put them into one file in a sort of flow.
# I'll try to find a way to set up auth and shit basically
#auth between the nick typing the command to the bot and the user_id associated with their kanboard auth
# TODO: Create interface between IRC and kamban for contributer management and executive functions
# assumptions: known user_nick, user_flags, known token and status for current bot sessions, request body, a nick -> kanban id dictionary is kept on the interface

class UserSession:
  """
  This class is for the management of active user sessions initiated by the bot.
  attrs:
    nick (String): username to be send responses to and whatnot
    user_id (int): passed in but retrieved from some other place, probably a master dictionary containing nick -> user_id in some other class or a global variable
    flag (String | int): irc botflag for user to check perms for stuff
    tokens and session status is managed elsewhere, probably same place as the master dictionary list. that would all be under authentication and will be handled later
  """

    def __init__(nick, flag):
      self.id = GLOB_ID_STORE[nick]
      self.flag = flag
      self.nick = nick

    def greet(num_tasks,num_overdue_tasks):
        return "Hello {nick}, welcome to your kanban dashboard. You currently have {n} assigned tasks, {n1} of which are overdue.".format(nick=self.nick,n=num_tasks, n1=num_overdue_tasks)

    def assigned_projects():
